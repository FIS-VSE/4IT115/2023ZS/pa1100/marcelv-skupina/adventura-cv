package cz.vse.java.xvalm00.adventuracv.observer;

public interface Observer {

    void update();

}
