package cz.vse.java.xvalm00.adventuracv.observer;

public interface Observable {

    void registerObserver(Observer observer);
    void notifyObservers();

}
