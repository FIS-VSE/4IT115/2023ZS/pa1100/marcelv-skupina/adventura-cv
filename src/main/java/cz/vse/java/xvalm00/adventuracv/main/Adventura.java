package cz.vse.java.xvalm00.adventuracv.main;

import cz.vse.java.xvalm00.adventuracv.gui.HerniPlocha;
import cz.vse.java.xvalm00.adventuracv.gui.PanelBatohu;
import cz.vse.java.xvalm00.adventuracv.gui.PanelVychodu;
import cz.vse.java.xvalm00.adventuracv.logika.HerniPlan;
import cz.vse.java.xvalm00.adventuracv.logika.Hra;
import cz.vse.java.xvalm00.adventuracv.logika.IHra;
import cz.vse.java.xvalm00.adventuracv.uiText.TextoveRozhrani;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;

public class Adventura extends Application {

    private final IHra hra = Hra.getSingleton();

    private ImageView viewVlka;
    private ImageView viewKarkulky;

    /***************************************************************************
     * Metoda, jejímž prostřednictvím se spouští celá aplikace.
     *
     * @param args parametry příkazového řádku
     */
    public static void main(String[] args) {
        if (args.length > 0 && args[0].equals("-gui")) {
            Application.launch();
        } else {
            IHra hra = Hra.getSingleton();
            TextoveRozhrani ui = new TextoveRozhrani(hra);
            ui.hraj();
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane borderPane = new BorderPane();

        TextArea konzole = new TextArea();
        borderPane.setCenter(konzole);
        konzole.setText(hra.vratUvitani());
        konzole.setEditable(false);

        TextField uzivatelskyVstup = pripravUzivatelskyVstup(konzole);

        Label popisekVstupu = new Label("Zadej příkaz: ");
        popisekVstupu.setFont(Font.font("Arial", FontWeight.BLACK, 12.0));

        HBox hBox = new HBox();
        hBox.getChildren().addAll(popisekVstupu, uzivatelskyVstup);
        hBox.setAlignment(javafx.geometry.Pos.CENTER);

        borderPane.setBottom(hBox);

        HerniPlocha herniPlocha = new HerniPlocha();
        borderPane.setTop(herniPlocha.getAnchorPane());

        PanelVychodu panelVychodu = new PanelVychodu(konzole);
        borderPane.setRight(panelVychodu.getListView());

        PanelBatohu panelBatohu = new PanelBatohu();
        borderPane.setLeft(panelBatohu.getFlowPane());

        VBox vBox = new VBox();
        MenuBar menuBar = new MenuBar();
        vBox.getChildren().addAll(menuBar, borderPane);

        Menu menuSoubor = new Menu("Soubor");
        MenuItem itemNovaHra = new MenuItem("Nová hra");
        MenuItem itemKonec = new MenuItem("Konec");
        menuSoubor.getItems().addAll(itemNovaHra, itemKonec);
        itemNovaHra.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                hra.restart();
                konzole.setText(hra.vratUvitani());
                konzole.appendText("\n" + hra.vratUvitani() + "\n");
                panelVychodu.update();
                panelBatohu.update();
                herniPlocha.update();
            }
        });
        itemKonec.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                System.exit(0);
            }
        });
        itemNovaHra.acceleratorProperty().set(javafx.scene.input.KeyCombination.keyCombination("Ctrl+N"));

        Menu menuNapoveda = new Menu("Nápověda");
        MenuItem itemOAplikaci = new MenuItem("O aplikaci");
        MenuItem itemDokumentace = new MenuItem("Dokumentace");
        menuNapoveda.getItems().addAll(itemOAplikaci, itemDokumentace);

        itemOAplikaci.setOnAction(actionEvent -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("O aplikaci");
            alert.setHeaderText("Adventura");
            alert.setContentText("Toto je adventura vytvořená v rámci semestrální práce předmětu 4IT115 - Programování II na VŠE v Praze.");
            alert.showAndWait();
        });

        itemDokumentace.setOnAction(actionEvent -> {
            WebView webView = new WebView();
//            webView.getEngine().load("https://java.vse.cz/4it115/Valovy");
            webView.getEngine().load(getClass().getResource("/napoveda.html").toExternalForm());
            Stage stage = new Stage();
            stage.setScene(new Scene(webView, 800, 600));
            stage.show();
        });

        menuBar.getMenus().addAll(menuSoubor, menuNapoveda);


        viewVlka = herniPlocha.getImageViewVlka();
        viewKarkulky = herniPlocha.getImageViewKarkulky();

        viewVlka.setX(100);
        viewVlka.setY(100);



        viewVlka.setBlendMode(BlendMode.COLOR_BURN);

        List<int[]> souradnice = new LinkedList<>();
        souradnice.add(new int[]{10, 40, 2});
        souradnice.add(new int[]{60, 100, 5});
        souradnice.add(new int[]{50, 150, 3});

        Executors.newSingleThreadExecutor().submit(
                () -> {
                        try {
                            for (int[] sourad : souradnice) {
                                pohybVlka(sourad[0], sourad[1], sourad[2]);
                                Thread.sleep(5000);
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                }
        );


        RotateTransition rotateTransition = new RotateTransition();
        rotateTransition.setNode(viewVlka);
        rotateTransition.setDuration(javafx.util.Duration.seconds(5));
        rotateTransition.setByAngle(360);
        rotateTransition.setCycleCount(1000);
        rotateTransition.setAutoReverse(true);
        rotateTransition.play();

        ScaleTransition scaleTransition = new ScaleTransition();
        scaleTransition.setNode(viewVlka);
        scaleTransition.setDuration(javafx.util.Duration.seconds(5));
        scaleTransition.setByX(2);
        scaleTransition.setByY(2);
        scaleTransition.setCycleCount(1000);
        scaleTransition.setAutoReverse(true);
        scaleTransition.play();




        Scene scene = new Scene(vBox);
        uzivatelskyVstup.requestFocus();

        primaryStage.setScene(scene);
        primaryStage.setTitle("Adventura");
        primaryStage.show();
    }

    private void pohybVlka(int x, int y, int time) {
        TranslateTransition translateTransition = new TranslateTransition();
        translateTransition.setNode(viewVlka);
        translateTransition.setDuration(javafx.util.Duration.seconds(time));
        translateTransition.setByX(x);
        translateTransition.setByY(y);
        //translateTransition.setAutoReverse(true);
        translateTransition.play();
    }

    private TextField pripravUzivatelskyVstup(TextArea konzole) {
        TextField uzivatelskyVstup = new TextField();
        uzivatelskyVstup.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                String prikaz = uzivatelskyVstup.getText();
                String odpoved = hra.zpracujPrikaz(prikaz);
                konzole.appendText("\n" + odpoved + "\n");
                uzivatelskyVstup.clear();
            }
        });
        return uzivatelskyVstup;
    }
}
