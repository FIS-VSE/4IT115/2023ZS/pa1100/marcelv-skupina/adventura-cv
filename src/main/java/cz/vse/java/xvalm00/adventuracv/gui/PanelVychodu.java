package cz.vse.java.xvalm00.adventuracv.gui;

import cz.vse.java.xvalm00.adventuracv.logika.HerniPlan;
import cz.vse.java.xvalm00.adventuracv.logika.Hra;
import cz.vse.java.xvalm00.adventuracv.logika.Prostor;
import cz.vse.java.xvalm00.adventuracv.observer.Observer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;

import java.util.ArrayList;
import java.util.Collection;

public class PanelVychodu implements Observer {

    private HerniPlan herniPlan = Hra.getSingleton().getHerniPlan();
    private Hra hra = Hra.getSingleton();
    private final ListView<String> listView = new ListView<>();
    private ObservableList<String> aktualniSeznamVychodu = FXCollections.observableList(new ArrayList<>());

    public PanelVychodu(TextArea konzole) {
        herniPlan.registerObserver(this);
        listView.setItems(aktualniSeznamVychodu);
        update();

        listView.setOnMouseClicked(event -> {
            String nazev = listView.getSelectionModel().getSelectedItem();
            String odpoved = hra.zpracujPrikaz("běž " + nazev);
            konzole.appendText("\n" + odpoved + "\n");
        });
    }

    public ListView<String> getListView() {
        return listView;
    }

    @Override
    public void update() {
        aktualniSeznamVychodu.clear();

        Prostor aktualniProstor = herniPlan.getAktualniProstor();
        Collection<Prostor> vychody = aktualniProstor.getVychody();
        for (Prostor prostor : vychody) {
            aktualniSeznamVychodu.add(prostor.getNazev());
        }
    }
}
